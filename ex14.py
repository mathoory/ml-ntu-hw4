import string
import numpy as np
from itertools import combinations_with_replacement
from liblinear.liblinearutil import *


def phi_q(X, Q):
    """Q-th order polynomial transform of input X containing inputs x_i by using combinatorics:
    find all possible index combinations of xi_1,...,xi_d, sort them,
    and use them to find value products.
    """
    return [[np.prod([xi[i] for i in index_combination])
             for total_degree in range(Q+1)
             for index_combination in sorted(set(combinations_with_replacement(range(len(xi)), total_degree)))]
            for xi in X]


if __name__ == "__main__":
    TIMES = 256

    X_train_D = np.genfromtxt("hw4_train.dat", usecols=range(10))
    y_train_D = np.genfromtxt("hw4_train.dat", usecols=10)

    # Transform and add 1s
    Q = 4  # Transform order
    X_train_D = phi_q(np.insert(X_train_D, 0, 1, axis=1), Q)

    lambdas_picked = [0, 0, 0, 0, 0]
    for i in range(TIMES):
        print(i)
        # Shuffle the indices instead of the data itself
        indices = np.random.permutation(len(X_train_D))
        train_indices = indices[:120]
        val_indices = indices[120:]

        X_train = [X_train_D[i] for i in train_indices]
        y_train = [y_train_D[i] for i in train_indices]
        X_val = [X_train_D[i] for i in val_indices]
        y_val = [y_train_D[i] for i in val_indices]

        best_lambda, best_err, num = -999, 1, None
        log10lambda = [-6, -3, 0, 3, 6]

        for i, log10l in enumerate(log10lambda):

            lambda_param = 10 ** log10l
            C = 1 / (2 * lambda_param)

            model = train(y_train, X_train, f"-s 0 -c {str(C)} -p 0.000001 -q")
            p_labs, p_acc, p_vals = predict(y_val, X_val, model)
            err = 1 - (p_acc[0] / 100)

            if (err == best_err and log10l > best_lambda) or (err < best_err):
                best_err, best_lambda, num = err, log10l, i

        lambdas_picked[num] += 1  # Count as best

    print(lambdas_picked)
    tot_best = np.argmax(lambdas_picked)
    print(
        f"Most picked [{string.ascii_lowercase[tot_best]}] : {log10lambda[tot_best]}")
