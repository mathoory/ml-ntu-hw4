import string
import numpy as np
from itertools import combinations_with_replacement
from liblinear.liblinearutil import *


def phi_q(X, Q):
    """Q-th order polynomial transform of input X containing inputs x_i by using combinatorics:
    find all possible index combinations of xi_1,...,xi_d, sort them,
    and use them to find value products.
    """
    return [[np.prod([xi[i] for i in index_combination])
             for total_degree in range(Q+1)
             for index_combination in sorted(set(combinations_with_replacement(range(len(xi)), total_degree)))]
            for xi in X]


if __name__ == "__main__":
    TIMES = 256

    X_train = []
    y_train = []
    X_test = []
    y_test = []
    Q = 4  # Transform order

    with open("hw4_train.dat", "r") as data:
        for line in data:
            line = line.strip()
            values = line.split()
            X_train.append(np.array([1]+values[0:10], dtype=float))
            y_train.append(np.float64(values[10]))

    with open("hw4_test.dat", "r") as data:
        for line in data:
            line = line.strip()
            values = line.split()
            X_test.append(np.array([1]+values[0:10], dtype=float))
            y_test.append(np.float64(values[10]))

    X_train_D = phi_q(np.array(X_train), Q)
    y_train_D = np.array(y_train)

    X_test = phi_q(np.array(X_test), Q)
    y_test = np.array(y_test)

    E_out = []
    for i in range(TIMES):
        print(i)
        # Shuffle the indices instead of the data itself
        indices = np.random.permutation(len(X_train_D))
        train_indices = indices[:120]
        val_indices = indices[120:]

        X_train = [X_train_D[i] for i in train_indices]
        y_train = [y_train_D[i] for i in train_indices]
        X_val = [X_train_D[i] for i in val_indices]
        y_val = [y_train_D[i] for i in val_indices]

        best_lambda = -999
        best_err = 1
        best_lambda_index = None
        log10lambda = [-6, -3, 0, 3, 6]
        models = []

        # Find the best model
        for i, log10l in enumerate(log10lambda):
            lambda_param = 10 ** log10l
            C = 1 / (2 * lambda_param)

            model = train(y_train, X_train, f"-s 0 -c {str(C)} -p 0.000001 -q")
            models.append(model)
            p_labs, p_acc, p_vals = predict(y_val, X_val, model)
            err = 1 - (p_acc[0] / 100)

            if (err == best_err and log10l > best_lambda) or (err < best_err):
                best_err = err
                best_lambda = log10l
                best_lambda_index = i

        # Use to predict on test set
        lambda_param = 10 ** best_lambda
        C = 1 / (2 * lambda_param)
        model_ultima = train(y_train_D, X_train_D,
                             f"-s 0 -c {str(C)} -p 0.000001 -q")
        p_labs, p_acc, p_vals = predict(y_test, X_test, model_ultima)
        E_out.append(1-(p_acc[0]/100))

    print(f"\n Avg. Eout: {np.mean(E_out)}")
