import string
import numpy as np
from itertools import combinations_with_replacement
from liblinear.liblinearutil import *


def phi_q(X, Q):
    """Q-th order polynomial transform of input X containing inputs x_i by using combinatorics:
    find all possible index combinations of xi_1,...,xi_d, sort them,
    and use them to find value products.
    """
    return [[np.prod([xi[i] for i in index_combination])
             for total_degree in range(Q+1)
             for index_combination in sorted(set(combinations_with_replacement(range(len(xi)), total_degree)))]
            for xi in X]


if __name__ == "__main__":
    # Load data and randomly split into Dtrain and Dval
    TIMES = 256
    FOLDS = 5

    X_train_D = np.genfromtxt("hw4_train.dat", usecols=range(10))
    y_train_D = np.genfromtxt("hw4_train.dat", usecols=10)

    # Transform and add 1s
    Q = 4  # Transform order
    X_train_D = phi_q(np.insert(X_train_D, 0, 1, axis=1), Q)

    # Split data into five folds
    fold_size, fold_indices = int(len(X_train_D)/FOLDS), []
    for i in range(FOLDS):
        start = i * fold_size
        end = start + fold_size
        fold_indices.append(list(range(start, end)))

    Ecv_list = []
    for i in range(TIMES):
        print(i)

        best_lambda, best_err, num = -999, 1, None
        log10lambda = [-6, -3, 0, 3, 6]
        for i, log10l in enumerate(log10lambda):
            lambda_param = 10 ** log10l
            C = 1 / (2 * lambda_param)

            total_err = 0
            for fold_index in fold_indices:
                # Split data into training and validation sets
                train_indices = [j for j in range(
                    len(X_train_D)) if j not in fold_index]
                val_indices = fold_index

                X_train = [X_train_D[j] for j in train_indices]
                y_train = [y_train_D[j] for j in train_indices]
                X_val = [X_train_D[j] for j in val_indices]
                y_val = [y_train_D[j] for j in val_indices]

                # Train model and calculate Ecv
                model = train(y_train, X_train,
                              f"-s 0 -c {str(C)} -p 0.000001 -q")
                p_labs, p_acc, p_vals = predict(y_val, X_val, model)
                err = 1 - (p_acc[0] / 100)
                total_err += err

            Ecv = total_err / FOLDS
            if (Ecv == best_err and log10l > best_lambda) or (Ecv < best_err):
                best_err, best_lambda, num = Ecv, log10l, i

        Ecv_list.append(best_err)

    print(f"Average Ecv(lambda*): {np.mean(Ecv_list)}")
