import string
import numpy as np
from itertools import combinations_with_replacement
from liblinear.liblinearutil import *


def phi_q(X, Q):
    """Q-th order polynomial transform of input X containing inputs x_i by using combinatorics:
    find all possible index combinations of xi_1,...,xi_d, sort them,
    and use them to find value products.
    """
    return [[np.prod([xi[i] for i in index_combination])
             for total_degree in range(Q+1)
             for index_combination in sorted(set(combinations_with_replacement(range(len(xi)), total_degree)))]
            for xi in X]


if __name__ == "__main__":
    X_train = np.genfromtxt("hw4_train.dat", usecols=range(10))
    y_train = np.genfromtxt("hw4_train.dat", usecols=10)
    X_test = np.genfromtxt("hw4_test.dat", usecols=range(10))
    y_test = np.genfromtxt("hw4_test.dat", usecols=10)

    # Transform and add 1s
    Q = 4  # Transform order
    X_train = phi_q(np.insert(X_train, 0, 1, axis=1), Q)
    X_test = phi_q(np.insert(X_test, 0, 1, axis=1), Q)

    best_lambda, best_err, letter = -999, 1, 'z'
    log10lambda = [-6, -3, 0, 3, 6]
    for i, log10l in enumerate(log10lambda):

        lambda_param = 10 ** log10l
        C = 1/lambda_param

        model = train(y_train, X_train,  f"-s 6 -c {str(C)} -p 0.000001 -q")
        p_labs, p_acc, p_vals = predict(y_test, X_test, model)
        err = 1-(p_acc[0]/100)

        if (err == best_err and log10l > best_lambda) or (err < best_err):
            best_err, best_lambda, letter = err, log10l, string.ascii_lowercase[i]

    print(f"\n argmin: [{letter}]: {best_lambda} (Err={best_err})")
